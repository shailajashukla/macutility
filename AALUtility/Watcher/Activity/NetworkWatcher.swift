//
//  NetworkAvailabilityController.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
import Cocoa
import SystemConfiguration
class NetworkWatcher{
    
    var dataHandler=NetworkDataHandler()
    
    
    func startWatcher(){
        var isConnected=NameOfAtivity.NetworkDown.rawValue
        if(connectedToNetwork()){
            isConnected=NameOfAtivity.NetworkUp.rawValue
        }
        
        dataHandler.prepareDataForActivity(data: isConnected)
    }
    
    func connectedToNetwork() -> Bool {

            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
            zeroAddress.sin_family = sa_family_t(AF_INET)
            guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                    SCNetworkReachabilityCreateWithAddress(nil, $0)
                }
            }) else {
                return false
            }
            var flags: SCNetworkReachabilityFlags = []
            if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
            }
            let isReachable = flags.contains(.reachable)
            let needsConnection = flags.contains(.connectionRequired)
            return (isReachable && !needsConnection)
        }

}

