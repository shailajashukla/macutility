//
//  SystemActivityWatcher.swift
//  AALUtility
//
//  Created by shailaja on 14/12/21.
//

import Foundation
import Combine
import AppKit
class SystemActivityWatcher{
    var dataHandler=CurrentActivityDataHandler()
    
    var bag = Set<AnyCancellable>()
    
    func startWatcher(){
        
        let dnc = DistributedNotificationCenter.default().publisher(for:  Notification.Name(rawValue: "com.apple.screenIsLocked")).sink{ _ in
            self.lock()
        }.store(in: &self.bag)
        
        DistributedNotificationCenter.default().publisher(for: Notification.Name(rawValue: "com.apple.screenIsUnlocked"))
            .sink { _ in
                self.unlock()
            }.store(in: &self.bag)
        //        NSWorkspace.shared.notificationCenter.addObserver(
        //            self, selector: #selector(onWakeNote(note:)),
        //            name: NSWorkspace.didWakeNotification, object: nil)
        //
        //        NSWorkspace.shared.notificationCenter.addObserver(
        //            self, selector: #selector(onSleepNote(note:)),
        //            name: NSWorkspace.willSleepNotification, object: nil)
        
        NSWorkspace.shared.notificationCenter.addObserver(
            self, selector: #selector(sessionDidBecomeActiveNotification(note:)),
            name: NSWorkspace.sessionDidBecomeActiveNotification, object: nil)
        
        NSWorkspace.shared.notificationCenter.addObserver(
            self, selector: #selector(sessionDidResignActiveNotification(note:)),
            name: NSWorkspace.sessionDidResignActiveNotification, object: nil)
        NSWorkspace.shared.notificationCenter.addObserver(
            self,
            selector: #selector(spaceChanged),
            name: NSWorkspace.activeSpaceDidChangeNotification,
            object: nil
        )
        
    }
    
    
    
    @objc func spaceChanged(note: NSNotification) {
        dataHandler.prepareDataForActivity(data: NameOfAtivity.Logout.rawValue)
        print("switch user")
    }
    @objc func sessionDidResignActiveNotification(note: NSNotification) {
        dataHandler.prepareDataForActivity(data: NameOfAtivity.Logout.rawValue)
        print("sessionDidResignActiveNotification")
    }
    
    @objc func sessionDidBecomeActiveNotification(note: NSNotification) {
        dataHandler.prepareDataForActivity(data: NameOfAtivity.Login.rawValue)
        print("sessionDidBecomeActiveNotification")
    }
    
    func lock() {
        dataHandler.prepareDataForActivity(data: NameOfAtivity.Lock.rawValue)
        print("lock")
    }
    
    func unlock() {
        dataHandler.prepareDataForActivity(data: NameOfAtivity.UnLock.rawValue)
        print("unlock")
    }
    
}
