//
//  Utility.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
public class Utility {
    
    static func getUrlComponents(url:String)->(host: String, scheme: String,port:Int?,path:String){
    
    let array = url.components(separatedBy: ":")
    var urlHost=array[1].replacingOccurrences(of: "//", with: "")
    let urlScheme=array[0]
    var urlPort:Int?=nil
    var urlPath:String=""
    if(array.count>1){
        var host=urlHost.components(separatedBy: "/")
        if(host.count>0){
            urlHost=host[0]
        }
                for var i in (1..<host.count)
                {
                    if(host[i] != ""){
                    urlPath="\(urlPath)/\(host[i])"
                    }
                }
//        if(host.count>1){
//            urlPath="/\(host[1])"
//        }
        let path  = (urlHost as NSString).deletingLastPathComponent
        //  let ext : NSString = str.lastPathComponent
        
        print(path)
        // print(ext)
    }
    if(array.count>2){
        let array1 = array[2].components(separatedBy: "/")
        urlPort=Int(array1[0])
        if(urlPath==""&&array1.count>1){
           let test=array1[0] as? String ?? ""
            if(test != ""){
            let path="/\(array1[1])"
            urlPath=path
            }
        }
    }
    
    return (urlHost,urlScheme,urlPort,urlPath)
}
}
extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
