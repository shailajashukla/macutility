//
//  UserDataModel.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
struct UserDataModel{
    var NTId:String=""
    var FusionId:String=""
    var Domain:String=""
    var IP:String=""
    var HostName:String=""
    var Version:String=""
}
