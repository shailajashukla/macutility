//
//  Constant.swift
//  AALUtility
//
//  Created by shailaja on 15/12/21.
//

import Foundation
enum NameOfAtivity:String{
    case Login="Login"
    case Logout="Logout"
    case Lock="Lock"
    case UnLock="UnLock"
    case NetworkUp="NetworkUp"
    case NetworkDown="NetworkDown"
}
