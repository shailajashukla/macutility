//
//  ViewController.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Cocoa

class MainController {
    
    public static var sharedInstance = MainController()
    
    var activityWatcher:ActivityWatcher?
    var datawatcher:DataWatcher?
    
     func initializeSetup(){
       
         initializeObjects()
         initializeUserData()
         initializePreviousActivities()
         initializeWatchers()
       
    }
    func initializeObjects(){
        activityWatcher=ActivityWatcher()
        datawatcher=DataWatcher()
    }
   
    func initializeUserData(){
        
    }
    func initializeWatchers(){
       
        activityWatcher?.startWatchingAllActivity()
        datawatcher?.startWatcher()
    }
    func initializePreviousActivities(){
        // current activity
    }
    
    
}

