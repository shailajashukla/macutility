//
//  AppActivityWatcher.swift
//  AALUtility
//
//  Created by shailaja on 14/12/21.
//

import Foundation
class AppActivityWatcher{
    
    var dataHandler=CurrentActivityDataHandler()
func startWatcher(){
    
    AppDataModel.sharedInstance.timerDataModel.acivityWatcherTimer = Timer.scheduledTimer(timeInterval: AppDataModel.sharedInstance.timerDataModel.acivityWatcherInterval, target: self, selector: #selector(self.checkForActivity), userInfo: nil, repeats: true)
  
}
    @objc  func checkForActivity(){
        dataHandler.prepareDataForActivity(data: "")
    }
}
