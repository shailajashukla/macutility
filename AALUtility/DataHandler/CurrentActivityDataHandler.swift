//
//  ActivityDataHandler.swift
//  AALUtility
//
//  Created by shailaja on 16/12/21.
//

import Cocoa

class CurrentActivityDataHandler: DataHandler {
    
    var currentActivityDBController=CurrentActivityDBController()
    
    func prepareDataForActivity(data:String){
        setUserDataForActivity()
        checkForPreviousActivity()
        updateStatusOnCurrentTable()
        updatePreviousTableWithRecords(records:["":""])
    }
    
    func checkForPreviousActivity(){
        
    }
    func updateStatusOnCurrentTable(){
        currentActivityDBController.insertData(records: ["":""])
    }
}
