//
//  ConnectionController.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation


import Cocoa
typealias CompletionBlock = (_ response: Any?, _ error: Error?) -> Void

class NetworkConnection: NSObject {
    
    var urlRequest:URLRequest!
    var isIP:Bool=false
    
    
    func setUrlParameterForGetRequest(urlParameter:[String:String])->[URLQueryItem]{
        var  parameter=[URLQueryItem]()
        for (key, value) in urlParameter {
            parameter.append(URLQueryItem(name: key, value: value))
        }
        
        
        return parameter
    }
    func setUrlParameterForPostRequest(urlParameter:[String:Any]){
        var parameter=[String:Any]()
        for (key, value) in urlParameter {
            parameter[key]=value
        }
        
        
        var  jsonData = NSData()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: urlParameter, options: .prettyPrinted) as NSData
            
            urlRequest.httpBody=jsonData as Data
            
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    func setUrlComponents(hostName:String,port:Int?,scheme:String,path:String,parameter:Any?,httpMethod:String){
        
        if(("GET").elementsEqual(httpMethod)){
            self.setUrlRequestForGetRequest(hostName: hostName,port: port, scheme: scheme, path: path, parameter: parameter)
        }
        else{
            self.setUrlRequestForPostRequest(hostName: hostName,port: port, scheme: scheme, path: path, parameter: parameter)
        }
        urlRequest.httpMethod=httpMethod
    }
    
    func setUrlRequestForGetRequest(hostName:String,port:Int?,scheme:String,path:String,parameter:Any?){
        var urlComp=URLComponents()
        urlComp.host=hostName
        urlComp.scheme=scheme
        if (port != nil){
            urlComp.port=port
        }
        urlComp.path=path
        if let urlParameter = parameter {
            urlComp.queryItems=setUrlParameterForGetRequest(urlParameter: urlParameter as! [String : String])
        }
        print("======== \(urlComp.url) ")
        urlRequest=URLRequest(url: urlComp.url!)
    }
    
    func setUrlRequestForPostRequest(hostName:String,port:Int?,scheme:String,path:String,parameter:Any?){
        var urlComp=URLComponents()
        urlComp.host=hostName
        urlComp.scheme=scheme
        urlComp.path=path
        
        if (port != nil){
            urlComp.port=port
        }
        urlRequest=URLRequest(url: urlComp.url!)
        
        if let urlParameter = parameter {
            
            setUrlParameterForPostRequest(urlParameter: urlParameter as! [String : Any]) }
        
        
    }
    
    func setUrlRequestForIP(hostName:String,port:Int?,scheme:String,path:String,parameter:Any?,httpMethod:String)  {
        let base = NSURL(string: "\(scheme)://\(hostName)")
        
        // let url = NSURL(string: path, relativeTo: base as URL?)
        var urlComp=URLComponents()
        urlComp.host="10.87.222.58"
        urlComp.scheme=scheme
        urlComp.path=path
        urlComp.port=8106
        urlRequest=URLRequest(url: urlComp.url!)
        // urlRequest = URLRequest(url: URL(string: url?.absoluteString ?? "")!)
        if let urlParameter = parameter {
            setUrlParameterForPostRequest(urlParameter: urlParameter as! [String : Any])
            
        }
        urlRequest.httpMethod=httpMethod
    }
    func setHeaderOfUrlRequest(key:String,value:String){
        urlRequest.setValue(value, forHTTPHeaderField: key)
        
    }
    
    
    func sendASynchronousRequesta(callBack: @escaping (Result<Any, Error>) -> Void){
        print(urlRequest)
        
        
        
        URLSession.shared.dataTask(with: urlRequest){(data,response,error) in
            if let httpResponse = response as? HTTPURLResponse {
                print("API status: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else {
                DispatchQueue.main.async {
                    
                }
                callBack(.failure(error!))
                return
            }
            
            do {
                
                
                let json = try JSONSerialization.jsonObject(with: validData, options: [])
                print(json)
                
                callBack(.success(json))
            } catch let serializationError {
                do{
                    var dataArray:Any?=nil
                    var errorInParsing=false
                    var orignalStr = String(decoding: validData, as: UTF8.self)
                    // print(str)
                    var str=orignalStr.replacingOccurrences(of: "\\", with: "")
                    //print(str)
                    str=str.replacingOccurrences(of: "\"{", with: "{")
                    str=str.replacingOccurrences(of: "}\",", with: "},")
                    str=str.replacingOccurrences(of: "}\"}}\"", with: "}}}")
                    
                    print(str)
                    let data1 = str.data(using: .utf8)!
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data1, options: .allowFragments) as? [String:AnyObject]
                        {
                            dataArray=jsonArray
                            print(jsonArray) // use the json here
                        } else {
                            errorInParsing=true
                            print("bad json")
                        }
                    } catch let error as NSError {
                        errorInParsing=true
                        print(error)
                    }
                    if(errorInParsing){
                        if ((orignalStr.hasPrefix("\"")) != nil) { // true
                            orignalStr = String(orignalStr.dropFirst().dropLast())
                            dataArray=orignalStr
                        }
                    }
                    callBack(.success(dataArray))
                }catch{
                    // print(validData)
                    callBack(.failure(serializationError))
                }
            }
        }.resume()
        
    }
    
    func sendASynchronousRequest(callBack: @escaping (Result<Any, Error>) -> Void){
        print(urlRequest)
        
        
        
        URLSession.shared.dataTask(with: urlRequest){(data,response,error) in
            if let httpResponse = response as? HTTPURLResponse {
                print("API status: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else {
                DispatchQueue.main.async {
                    
                }
                callBack(.failure(error!))
                return
            }
            
            do {
                
                
                let json = try JSONSerialization.jsonObject(with: validData, options: [])
                print(json)
                
                callBack(.success(json))
            } catch let serializationError {
                do{
                    var dataArray:Any?=nil
                    var errorInParsing=false
                    var orignalStr = String(decoding: validData, as: UTF8.self)
                    // print(str)
                    var str=orignalStr.replacingOccurrences(of: "\\", with: "")
                    //print(str)
                    str=str.replacingOccurrences(of: "\"{", with: "{")
                    str=str.replacingOccurrences(of: "}\",", with: "},")
                    str=str.replacingOccurrences(of: "}\"}}\"", with: "}}}")
                    
                    print(str)
                    let data1 = str.data(using: .utf8)!
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data1, options: .allowFragments) as? [String:AnyObject]
                        {
                            dataArray=jsonArray
                            print(jsonArray) // use the json here
                        } else {
                            errorInParsing=true
                            print("bad json")
                        }
                    } catch let error as NSError {
                        errorInParsing=true
                        print(error)
                    }
                    if(errorInParsing){
                        if ((orignalStr.hasPrefix("\"")) != nil) { // true
                            orignalStr = String(orignalStr.dropFirst().dropLast())
                            dataArray=orignalStr
                        }
                    }
                    callBack(.success(dataArray))
                }catch{
                    // print(validData)
                    callBack(.failure(serializationError))
                }
            }
        }.resume()
        
    }
    
    
    
    
    
    
    
    func sendSynchronousRequest(callBack: @escaping (Result<Any, Error>) -> Void){
        
        
        
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: urlRequest){(data,response,error) in
            if let httpResponse = response as? HTTPURLResponse {
                print("API status: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else {
                DispatchQueue.main.async {
                }
                
                callBack(.failure(error!))
                semaphore.signal()
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: validData, options: [.allowFragments      ]) as! [String:AnyObject]
                print(json)
                
                var a = validData.map { String(format: "%02x", $0) }.joined(separator: "    ")
                
                print(a)
                
                callBack(.success(json))
            } catch let serializationError {
                do{
                    var str = String(decoding: validData, as: UTF8.self)
                    
                    if ((str.hasPrefix("\"")) != nil) { // true
                        str = String(str.dropFirst().dropLast())
                    }
                    
                    callBack(.success(str))
                }catch{
                    // print(validData)
                    callBack(.failure(serializationError))
                }
            }
            semaphore.signal()
        }.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        
        
    }
}
