//
//  CurrentActivityDataModel.swift
//  AALUtility
//
//  Created by shailaja on 16/12/21.
//

import Foundation
struct CurrentActivityDataModel{
 public var activeWindow: String?
 public var activeWindowTitle: String?
 public var activity: String?
 public var domain: String?
 public var flag: String?
 public var fusionID: String?
 public var hostName: String?
 public var ip: String?
 public var ntID: String?
 public var processStartTime: String?
 public var reportDate: String?
 public var startTime: String?
 public var version: String?
}
