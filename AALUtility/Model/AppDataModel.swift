//
//  ConfigurableDataModel.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
struct TimerDataModel{
    var acivityWatcherTimer:Timer?=nil
    var apiTimer:Timer?=nil
    var networkCheckTimer:Timer?=nil
    
    var acivityWatcherInterval:TimeInterval=10
    var apiTimeInterval:TimeInterval=10
    var networkCheckTimeInterval:TimeInterval=10
}
struct AppDataModel{
    public static var sharedInstance = AppDataModel()
    
    var userDatamodel=UserDataModel()
    var currentAppActivityDataModel=CurrentActivityDataModel()
    var currentNetworkActivityDataModel=CurrentActivityDataModel()
    var timerDataModel=TimerDataModel()
    
}
