//
//  DataWatcher.swift
//  AALUtility
//
//  Created by shailaja on 14/12/21.
//

import Foundation
class DataWatcher{
    var dbController=PreviousActivitiesDBController()
    var serverConnection=AWSServerConnectionController()
    
 

func startWatcher(){
    
    AppDataModel.sharedInstance.timerDataModel.acivityWatcherTimer = Timer.scheduledTimer(timeInterval: AppDataModel.sharedInstance.timerDataModel.acivityWatcherInterval, target: self, selector: #selector(self.checkForData), userInfo: nil, repeats: true)
  
}
    
    @objc func checkForData(){
      let data=dbController.fetchData()
        if(true){
            serverConnection.sendDataToServer(data: ["":""])
        }
    }
}
