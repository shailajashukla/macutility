//
//  NetworkDataHandler.swift
//  AALUtility
//
//  Created by shailaja on 16/12/21.
//

import Cocoa

class NetworkDataHandler: DataHandler {
    
     var networkActivityDBCotroller=NetworkActivityDBCotroller()
    
    func prepareDataForActivity(data:String){
        setUserDataForActivity()
        checkForPreviousActivity()
        updateStatusOnCurrentTable()
        updatePreviousTableWithRecords(records: ["":""])
    }
    
    func checkForPreviousActivity(){
        
    }
    func updateStatusOnCurrentTable(){
        //prepare the data
        networkActivityDBCotroller.insertData(records: ["":""])
    }
}
