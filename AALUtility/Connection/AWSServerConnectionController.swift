//
//  AWSServerConnectionController.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
class AWSServerConnectionController{
    var dataHandler=PreviousActivitiesDataHandler()
    
    func sendDataToServer(data:[String:Any]){
        
//        let a = ["starttimes":"2021-09-16 07:54:08","reportdate":"2021-09-16 03:16:12","processstarttime":"2021-09-16 05:17:47","hostname":"l-101712571","ip":"192.168.1.11","version":"3.0.8","flag":"s","sendretrycount":-1,"iscurrentactivity":true,"duration":16,"idelduration":0,"activity":"chrome.exe (agent activity logger - google chrome)"] as [String : Any]
        
       let parameter=setParameter(data: data)
           

        let centralUrl = "https://44yjiddai2.execute-api.eu-west-1.amazonaws.com/AAL_TEST_SendAgentDataToRedshift_New/"
        let result=Utility.getUrlComponents(url: centralUrl)
        let networkConnection = NetworkConnection()
        networkConnection.setUrlComponents(
            hostName: result.host,//urlConstants.UrlHostName108106,
            port: result.port,//urlConstants.UrlPort8106,
            scheme: result.scheme, //urlConstants.urlHttpScheme,
            path: "\(result.path)"+"/sendagentdatatoredshift",
            parameter: parameter,
            httpMethod: "POST" )
   

        networkConnection.setHeaderOfUrlRequest(key: "Accept", value: "application/json")
        networkConnection.setHeaderOfUrlRequest(key: "Content-Type", value: "application/json")
        
        networkConnection.setHeaderOfUrlRequest(key: "x-api-key", value: "AAL_TEST_SendAgentDataToRedshift_New_Key")
        networkConnection.sendASynchronousRequest { [self] (result) in


            switch result {
            case .success(let gists):
                
                // print(gists)
                if let temp=gists as? [String:Any]{
                    self.dataHandler.deleteOlderRecords()
                    print(temp)
                }
                
            
        case .failure(let error):
            print(error)
                self.dataHandler.updateStatusOfPreviousActivity()
            
        }
                

        }
        
        
        
    }
    func setParameter(data:[String:Any])->[String : Any]{
        let Records = ["Data":data]
        
        let dictionary = ["DeliveryStreamName":"AAL_TEST_SendAgentDataToRedshift","Records":[Records]] as [String : Any]
        
      
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dictionary,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        return dictionary
    }
    
}
