//
//  DBController.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation
protocol curdOperationOnDB{
    func fetchData()
    func insertData(records:[String:Any])
    func deleteData()
    func updateData()
}

