//
//  ActivityDataModel.swift
//  AALUtility
//
//  Created by shailaja on 13/12/21.
//

import Foundation
struct ActivityDataModel{
    
    var ReportDate:String=""
    var Duration:String=""
    var Flag:String=""
    var ProcessStartTime:String=""
    var Activity:String=""
    var ActiveWindow:String=""
    var ActiveWindowTitle:String=""
    var BrowserUrl:String=""
    var LastSeen:String=""
    var CreatedDate:String=""
    
}
