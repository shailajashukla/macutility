//
//  CaptureActivity.swift
//  AALUtility
//
//  Created by Shailaja shukla  on 13/12/21.
//

import Foundation

class ActivityWatcher {
    
    var systemActivityWatcher=SystemActivityWatcher()
    var appActivityWatcher=AppActivityWatcher()
    var networkWatcher=NetworkWatcher()
    
    
    init(){
     print("init")
    }
     func startWatchingAllActivity(){
        systemActivityWatcher.startWatcher()
        networkWatcher.startWatcher()
        appActivityWatcher.startWatcher()
    }
 
    

}
